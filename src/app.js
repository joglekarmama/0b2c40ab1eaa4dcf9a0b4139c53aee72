import { size } from 'lodash';
import React from 'react';
import config from 'visual-config-exposer'
import Main from './components/Main'


const App = () => {
 
  const image=config.settings.image
  
  /*document.body.style.background="url("+image+")"*/
 /* document.body.style.backgroundSize='cover'
  document.body.style.backgroundPosition='cover'
  document.body.style.backgroundRepeat='no-repeat'*/
  return (
    <div>
     <img className='image' src={image}/>
      <Main />
    </div>
  );
};

export default App;
